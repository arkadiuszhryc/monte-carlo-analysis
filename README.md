Monte Carlo option analysis with C++
====================================

**This project uses C++ std library solutions for pricing options with the Monte
Carlo method.** At the current time only vanilla options are supported.

Project under MIT licence.