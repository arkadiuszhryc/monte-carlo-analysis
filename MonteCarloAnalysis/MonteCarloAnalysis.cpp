#include "stdafx.h"
#include <iostream>
#include <chrono>
#include <random>
#include <vector>
#include <math.h>

float monteCarlo(int t, double s, double r, double q, double sigma) {
	// Random number generator:
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);
	std::normal_distribution<double> distribution(0.0, 1.0);

	const int numOfTrajectories = 100000;
	const int numOfDays = t;

	// A creation of matrix for trajectory analysis:
	double corrA = sigma;
	double corrB = ((r - q - (pow(sigma, 2) / 2)));
	double temp = 0;
	double st = 0;
	double sumsArr[numOfTrajectories] = {};

	for (int i = 0; i < numOfTrajectories; ++i) {
		for (int j = 0; j < numOfDays; ++j) {
			temp = distribution(generator);
			st += temp * corrA + corrB;
		}
		sumsArr[i] = s * exp(st);
		st = 0;
	}

	// Calculating mean
	double simulationMean = 0;
	for (int i = 0; i < numOfTrajectories; ++i) {
		simulationMean += (sumsArr[i] / numOfTrajectories);
	}

	// Calculating price
	double discountRate = -r * numOfDays / 365;
	double c = pow(simulationMean, discountRate); // Price of call option
	c = c * s;

	return c;
}

int main()
{
	double s; // Price of the base instrument today
	int t; // Time to maturity
	double r; // Risk-free interest rate
	double q; // Dividend yield
	double sigma; // Standard deviation of the stock's returns

	std::cout << "Hello! This little program will help you in pricing the european options." << std::endl;
	std::cout << "It uses the Monte Carlo method." << std::endl;
	std::cout << "Insert price of the underlying instrument: ";
	std::cin >> s;
	std::cout << "Insert time to maturity (in days): ";
	std::cin >> t;
	std::cout << "Insert risk-free interest rate: ";
	std::cin >> r;
	std::cout << "Insert dividend yield: ";
	std::cin >> q;
	std::cout << "Insert variability of the underlying instrument: ";
	std::cin >> sigma;

	double c = monteCarlo(t, s, r, q, sigma);
	std::cout << "Price of the call option: " << std::endl;
	std::cout << round(100 * c) / 100 << std::endl;

	system("pause");

	return 0;
}